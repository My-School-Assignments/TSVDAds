# This script contains base class for models
# So we have some common interface to use in all models


class Model(object):
    def __init__(self, features, label):
        self.features = features
        self.label = label

    def Train(self):
        raise NotImplementedError("Not implemented base method Train.")

    def Predict(self, evaluate):
        raise NotImplementedError("Not implemented base method Predict.")

    def Export(self, filename):
        raise NotImplementedError("Not implemented base method Export.")

    def Load(self, filename):
        raise NotImplementedError("Not implemented base method Export.")
