from Integration import Integrator, PreProcess
from Model import Model
from pyspark.ml import Pipeline, PipelineModel
from pyspark.ml.regression import LinearRegression, LinearRegressionModel
from pyspark.ml.feature import StringIndexer, VectorIndexer, VectorAssembler
from pyspark.ml.evaluation import RegressionEvaluator

class LinearLeastSquare(Model):
    def __init__(self, features, labels, integrator):
        super(LinearLeastSquare, self).__init__(features,labels)
        self.integrator = integrator
        # We want to know how much did people spend only if they have finished the transaction
        self.integrator.PreProcessed = PreProcess.GetPositiveRecords(integrator.PreProcessed)
        # For learning models, it is necessary to combine all input attributes into a single numeric vector using the 'VectorAssembler'
        self.data = VectorAssembler(inputCols = self.features, outputCol="features").transform(self.integrator.PreProcessed)
        # Index labels, adding metadata to the label column.
        # Fit on whole dataset to include all labels in index.
        self.labelIndexer = StringIndexer(inputCol=self.label, outputCol="indexedLabel").fit(self.data)

        # Automatically identify categorical features, and index them.
        # We specify maxCategories so features with > 4 distinct values are treated as continuous.
        self.featureIndexer = VectorIndexer(inputCol="features", outputCol="indexedFeatures", maxCategories=8).fit(self.data)

        # Split the data into training and test sets (30% held out for testing)
        (self.trainingData, self.testData) = self.data.randomSplit([0.7, 0.3])
        self.linearRegression = LinearRegression(labelCol="indexedLabel",featuresCol="indexedFeatures", maxIter=50, regParam= 0.1)
        self.pipeline = Pipeline(stages=[self.labelIndexer,self.featureIndexer,self.linearRegression])
        self.linearRegressionModel = None

    def Train(self):
        self.linearRegressionModel = self.pipeline.fit(self.trainingData)

    def Predict(self, evaluate):
        if evaluate:
            predictions = self.linearRegressionModel.transform(self.testData)
            evaluator = RegressionEvaluator(labelCol="indexedLabel",
                                            predictionCol="prediction",
                                            metricName="rmse")
            accuracy = evaluator.evaluate(predictions)
            return predictions.select("prediction", "indexedLabel", "features"), 1 - accuracy

        predictions = self.linearRegressionModel.transform(self.data)
        return predictions.select("prediction", "indexedLabel", "features")

    def Export(self, filename):
        self.linearRegressionModel.write().overwrite().save(filename)

    def Load(self, filename):
        self.linearRegressionModel = PipelineModel.load(filename)
