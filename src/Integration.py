# This file contains integration scripts
# Sampling, Loading etc. of data

import csv
import math
import sys
import urllib
import Utilities
from pyspark.sql import Row, DataFrame
from pyspark.sql.functions import col, when
from pyspark.ml.feature import StringIndexer, Imputer, QuantileDiscretizer

class Integrator:
    PreProcessed = None

    def __init__(self, sparkContext, sparkSession):
        self.sc = sparkContext
        self.spark = sparkSession
        self.loadData = None
        self.rawData = None

    def LoadRaw(self, dataPath):
        self.loadData = self.sc.textFile(dataPath)
        self.loadData = self.loadData.map(lambda line: line.split(","))
        self.rawData = self.loadData.map(lambda line: Row(
                Sale = float(line[0].encode('utf-8')),
                SalesAmnountInEuro = float(line[1].encode('utf-8')),
                Time_delay_for_conversion = float(line[2].encode('utf-8')),
                click_timestamp = str(line[3].encode('utf-8')),
                nb_clicks_1week = float(line[4].encode('utf-8')),
                product_price = str(line[5].encode('utf-8')),
                product_age_group = str(line[6].encode('utf-8')),
                device_type = str(line[7].encode('utf-8')),
                audience_id = str(line[8].encode('utf-8')),
                product_gender = str(line[9].encode('utf-8')),
                product_brand = str(line[10].encode('utf-8')),
                product_category1 = str(line[11].encode('utf-8')),
                product_category2=str(line[12].encode('utf-8')),
                product_category3=str(line[13].encode('utf-8')),
                product_category4=str(line[14].encode('utf-8')),
                product_category5=str(line[15].encode('utf-8')),
                product_category6=str(line[16].encode('utf-8')),
                product_category7=str(line[17].encode('utf-8')),
                product_country = str(line[18].encode('utf-8')),
                product_id =  str(line[19].encode('utf-8')),
                product_title = str(line[20].encode('utf-8')),
                partner_id = str(line[21].encode('utf-8')),
                user_id = str(line[22].encode('utf-8'))))

        self.rawData = self.spark.createDataFrame(self.rawData)

    def LoadPreProcessed(self, path):
        self.PreProcessed = self.spark.read.format("csv").option("header", "true").load(path)
        self.PreProcessed = self.PreProcessed.withColumn("Sale", self.PreProcessed["Sale"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("SalesAmnountInEuro", self.PreProcessed["SalesAmnountInEuro"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("Time_delay_for_conversion", self.PreProcessed["Time_delay_for_conversion"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("click_timestamp", self.PreProcessed["click_timestamp"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("nb_clicks_1week", self.PreProcessed["nb_clicks_1week"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_price", self.PreProcessed["product_price"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_age_group", self.PreProcessed["product_age_group"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("device_type", self.PreProcessed["device_type"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("audience_id", self.PreProcessed["audience_id"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_gender", self.PreProcessed["product_gender"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_brand", self.PreProcessed["product_brand"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_category1", self.PreProcessed["product_category1"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_category2", self.PreProcessed["product_category2"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_category3", self.PreProcessed["product_category3"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_category4", self.PreProcessed["product_category4"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_category5", self.PreProcessed["product_category5"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_category6", self.PreProcessed["product_category6"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_category7", self.PreProcessed["product_category7"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_country", self.PreProcessed["product_country"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_id", self.PreProcessed["product_id"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("product_title", self.PreProcessed["product_title"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("partner_id", self.PreProcessed["partner_id"].cast("double"))
        self.PreProcessed = self.PreProcessed.withColumn("user_id", self.PreProcessed["user_id"].cast("double"))

    def LoadRawFromHttp(self, address, name):
        self.rawData = urllib.urlretrieve(address, name)
        self.loadData = self.sc.textFile("./" + name )
        self.loadData = self.loadData.map(lambda line: line.split(","))
        self.rawData = self.loadData.map(lambda line: Row(
                Sale = float(line[0].encode('utf-8')),
                SalesAmnountInEuro = float(line[1].encode('utf-8')),
                Time_delay_for_conversion = float(line[2].encode('utf-8')),
                click_timestamp = str(line[3].encode('utf-8')),
                nb_clicks_1week = float(line[4].encode('utf-8')),
                product_price = str(line[5].encode('utf-8')),
                product_age_group = str(line[6].encode('utf-8')),
                device_type = str(line[7].encode('utf-8')),
                audience_id = str(line[8].encode('utf-8')),
                product_gender = str(line[9].encode('utf-8')),
                product_brand = str(line[10].encode('utf-8')),
                product_category1 = str(line[11].encode('utf-8')),
                product_category2=str(line[12].encode('utf-8')),
                product_category3=str(line[13].encode('utf-8')),
                product_category4=str(line[14].encode('utf-8')),
                product_category5=str(line[15].encode('utf-8')),
                product_category6=str(line[16].encode('utf-8')),
                product_category7=str(line[17].encode('utf-8')),
                product_country = str(line[18].encode('utf-8')),
                product_id =  str(line[19].encode('utf-8')),
                product_title = str(line[20].encode('utf-8')),
                partner_id = str(line[21].encode('utf-8')),
                user_id = str(line[22].encode('utf-8'))))

        self.rawData = self.spark.createDataFrame(self.rawData)

    def PreProcess(self):
        # Replace values "-1" with None
        self.rawData = PreProcess.ReplaceEmptyValues(self.rawData, "-1")

        # Transform nominal attributes to numeric attributes
        for columnName, columnType in self.rawData.dtypes:
            if columnType == "string":
                self.rawData = PreProcess.TransformNominalToNumeric(self.rawData, columnName)

        # Replace missing vales with mean
        self.PreProcessed = PreProcess.ProcessMissingValues(self.rawData)

    def GenerateStatistics(self, statFile):
        content = ""

        #Compute Statistics, Histograms and Ratio criterion
        for columnName, columnType in self.PreProcessed.dtypes:
            if columnType == "string":
                histogram = PreProcess.CreateHistogram(self.PreProcessed, columnName)
                entropy = PreProcess.CalculateEntropy(self.PreProcessed, columnName)
                content = content + "COLUMN NAME: " + columnName + "\n"
                content = content + "Column Type: " + columnType + "\n"
                content = content + "Histogram:\n" + "".join([str(i) + "\n" for i in histogram]) + "\n"
                content = content + "Entropy:\n" + str(entropy) + "\n\n\n\n\n"

            else:
                stats = PreProcess.ComputeStatistics(self.PreProcessed, columnName)
                content = content + "COLUMN NAME: " + columnName + "\n"
                content = content + "Column Type: " + columnType + "\n"
                content = content + "Statistics:\n" + "".join([str(i) + "\n" for i in stats]) + "\n\n\n\n\n"

        # Save statistics into text file
        with open(statFile, "w") as fp:
            fp.write(content)

    def Sample(self, numberOfData):
        self.PreProcessed = self.PreProcessed.sample(False, numberOfData)

    def SampleRaw(self, numberOfData):
        self.rawData = self.rawData.sample(False, numberOfData)

    def DivideTrainTest(self, numberOfTrain, numberOfTest):
        # type: (float, float) -> (DataFrame, DataFrame)
        return self.PreProcessed.randomSplit([numberOfTrain, numberOfTest], seed=31481641) # % 0.6, 0.4

    def SaveSampled(self, pathToSave):
        if not pathToSave.endswith("/")  or not pathToSave.endswith("\\"):
            pathToSave = pathToSave + "/"

        Utilities.silentremove(pathToSave + "Sampled.csv")
        self.rawData.toPandas().to_csv(pathToSave + "Sampled.csv")

    def SavePreProcessed(self, pathToSave):
        if not pathToSave.endswith("/")  or not pathToSave.endswith("\\"):
            pathToSave = pathToSave + "/"

        Utilities.silentremove(pathToSave + "PreProcessed.csv")
        self.PreProcessed.toPandas().to_csv(pathToSave + "PreProcessed.csv")

class PreProcess:
    @staticmethod
    def GetPositiveRecords(dataFrame):
        """Returns Data Frame with only positive records.
        - Attribute: Sale = 1"""
        positiveRecords = dataFrame.filter(dataFrame["Sale"] == 1)
        return positiveRecords

    @staticmethod
    def GetNegativeRecords(dataFrame):
        """Returns Data Frame with only negative records.
        - Attribute: Sale = 0"""
        positiveRecords = dataFrame.filter(dataFrame["Sale"] == 0)
        return positiveRecords

    @staticmethod
    def ReplaceEmptyValues(dataFrame, emptyValue):
        """Replacing empty values (-1) with Null (None)"""
        for columnName, columnType in dataFrame.dtypes:
            dataFrame = dataFrame.withColumn(columnName, PreProcess.EmptyValueAsNull(columnName, emptyValue))
        return dataFrame

    @staticmethod
    def EmptyValueAsNull(colName, emptyValue):
        return when(col(colName) != emptyValue, col(colName)).otherwise(None)

    @staticmethod
    def TransformNominalToNumeric(dataFrame, inputColumn):
        """Transformation of nominal attributes into numeric"""
        outputColumn = inputColumn + "_index"
        indexer = StringIndexer(inputCol = inputColumn, outputCol = outputColumn, handleInvalid = "keep").fit(dataFrame)
        dataFrame = (indexer
         .transform(dataFrame)
         .withColumn(outputColumn, when(col(outputColumn) == len(indexer.labels), None).otherwise(col(outputColumn))))
        dataFrame = dataFrame.drop(inputColumn)
        dataFrame = dataFrame.withColumnRenamed(outputColumn, inputColumn)
        return dataFrame

    @staticmethod
    def TransformNumericToNominal(dataFrame, inputColumn, numberOfBuckets):
        """Transformation of numeric attributes into nominal
        - numberOfBuckets should be the same integer as it is used in decission tree"""
        outputColumn = inputColumn + "_quantiled"
        discretizer = QuantileDiscretizer(numBuckets = numberOfBuckets, inputCol = inputColumn, outputCol = outputColumn)
        discretizer = discretizer.fit(dataFrame)
        dataFrame = discretizer.transform(dataFrame)
        dataFrame = dataFrame.drop(inputColumn)
        dataFrame = dataFrame.withColumnRenamed(outputColumn, inputColumn)
        return dataFrame

    @staticmethod
    def ComputeStatistics(dataFrame, column):
        """Calculating statistics for numeric attributes"""
        statsDataFrame = dataFrame.describe([column])
        statsList = statsDataFrame.collect()
        return statsList

    @staticmethod
    def CreateHistogram(dataFrame, column):
        """Creating histograms for nominal attributes"""
        histogram = dataFrame.groupBy(column).count().collect()
        return histogram

    @staticmethod
    def CalculateEntropy(dataFrame, column):
        """Calculation of the ratio criterion
        - The information gain to the target attribute (classification task)
        - Used for nominal attributes"""
        entropy = 0
        numElements = 0
        histogram = PreProcess.CreateHistogram(dataFrame, column)

        for count in histogram:
            numElements = numElements + count[1]

        for count in histogram:
            freq = float(count[1]) / float(numElements)
            if freq != 0:
                entropy = entropy - (freq * math.log(freq, 2))
        return entropy

    @staticmethod
    def ProcessMissingValues(dataFrame):
        """Replacing missing values with mean"""
        originalColumns = dataFrame.columns
        imputer = Imputer(inputCols = dataFrame.columns, outputCols = ["{0}_imputed".format(c) for c in dataFrame.columns])
        imputerModel = imputer.fit(dataFrame)
        dataFrame = imputerModel.transform(dataFrame)
        for columnName in originalColumns:
            dataFrame = dataFrame.drop(columnName)

        # Rename the columns to have their original names.
        for currentColumn in dataFrame.columns:
            dataFrame = dataFrame.withColumnRenamed(currentColumn, currentColumn.replace("_imputed",""))
        return dataFrame
