# Main script that runs the entire thing

import argparse
from Utilities import *
from Integration import Integrator
from Model import Model
from DecisionTreeModel import DecisionTreeModel
from LinearSVM import LinearSVM
from Bayes import Bayes
from GradientBoostedTrees import GradientBoostedTrees
from LinearLeastSquare import LinearLeastSquare

def PerformLoading(dataFile, dataType, preprocess, method, sample, savedata):
    # try:
    if dataFile is not None:
        if dataType is None or dataType.lower() == DataTypes.Load:
            if preprocess:
                integrator.LoadRaw(dataFile)
                if sample:
                    integrator.SampleRaw(0.1)
                integrator.PreProcess()
                if savedata is not None:
                    integrator.SavePreProcessed(savedata)
            else:
                integrator.LoadPreProcessed(dataFile)

        elif dataType.lower() == DataTypes.Download:
            if preprocess or (method is not None and method.lower() == "all"):
                integrator.LoadRawFromHttp(dataFile[:dataFile.rfind("/")], dataFile[dataFile.rfind("/")+1:])
                if sample:
                    integrator.SampleRaw(0.1)
                integrator.PreProcess()
            else:
                print("Sorry downloaded data would probably not be PreProcessed use -m PreProcess or -m all flag")
                exit(1)

        else:
            print("Wrong argument -DataType-.")
            print("You entered: " + dataType)
            exit(1)
    else:
        print("You MUST specify path to data file to use -> -f 'path/to/datafile.csf'")
        exit(1)

    # except Exception, e:
    #     print("Msg: " + str(e))
    #     print("Wrong argument -DataFile-")
    #     print("You entered: " + dataFile)
    #     exit(1)


if __name__ == "__main__":
    # Parse Arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--dataFile", help="Path to data file(s)")
    parser.add_argument("-t", "--dataType", help="Type of data: load, download")
    parser.add_argument("-m", "--method", help="Method: train, predict, all")
    parser.add_argument("-p", "--preprocess", action='store_true', help="Set this if data you are loading aren't preprocessed.")
    parser.add_argument("-stat", "--statistics", help="Compute statistics: specify path where to save results")
    parser.add_argument("-samp", "--sample", action='store_true', help="Set this if you want to sample data before preprocessing.")
    parser.add_argument("-sdat", "--savedata", help="Path were to save preprocessed data.")
    parser.add_argument("-i", "--modelInput", help="Full filename path to already trained model." )
    parser.add_argument("-md", "--model", help="Model to use: DecTree, linsvm, bayes, gbtclass, GBTRegPrice, GBTRegTime, LinLSPrice, LinLSTime")
    parser.add_argument("-o", "--modelOutput", help="Full filename path where to save trained model. If not set the model will not be saved!")
    args = parser.parse_args()

    # Init
    from pyspark.shell import spark
    integrator = Integrator(spark.sparkContext, spark)
    model = Model

    # Loads data based on Arguments
    if args.method is not None and args.method.lower() == "all":
        PerformLoading(args.dataFile, args.dataType, args.preprocess, args.method, args.sample, None)
    else:
        PerformLoading(args.dataFile, args.dataType, args.preprocess, args.method, args.sample, args.savedata)

    if args.modelInput is None and args.method.lower() == "predict":
        print("Bad combination of arguments. You can't predict if you haven't loaded any model.")
        exit(1)

    if args.statistics is not None:
        integrator.GenerateStatistics(args.statistics)

    if args.method is not None:
        if args.method.lower() == "all":

            # Create Decision Tree Model
            model = DecisionTreeModel(["nb_clicks_1week", "product_price",
                                       "product_age_group", "device_type",
                                       "audience_id", "product_gender",
                                       "product_brand", "product_category1",
                                       "product_category2", "product_category3",
                                       "product_category4", "product_category5",
                                       "product_category6", "product_category7",
                                       "product_title"], "Sale", integrator)
            model.Train()
            # model.Load(os.path.dirname(os.path.realpath(__file__)) + "/../Models/DecTree.model")
            predictions, accDecTree = model.Predict(True)
            model.Export(os.path.dirname(os.path.realpath(__file__)) + "/../Models/DecTree.model")

            # Create Linear SVM model
            model = LinearSVM(["nb_clicks_1week", "product_price",
                               "product_age_group", "device_type",
                               "audience_id", "product_gender",
                               "product_brand", "product_category1",
                               "product_category2", "product_category3",
                               "product_category4", "product_category5",
                               "product_category6", "product_category7",
                               "product_title"], "Sale", integrator)
            model.Train()
            # model.Load(os.path.dirname(os.path.realpath(__file__)) + "/../Models/LinSVM.model")
            predictions, accLinSVM = model.Predict(True)
            model.Export(os.path.dirname(os.path.realpath(__file__)) + "/../Models/LinSVM.model")

            # Bayes Classifier
            model = Bayes(["nb_clicks_1week", "product_price",
                           "product_age_group", "device_type",
                           "audience_id", "product_gender",
                           "product_brand", "product_category1",
                           "product_category2", "product_category3",
                           "product_category4", "product_category5",
                           "product_category6", "product_category7",
                           "product_title"], "Sale", integrator)
            model.Train()
            # model.Load(os.path.dirname(os.path.realpath(__file__)) + "/../Models/Bayes.model")
            predictions, accBayes = model.Predict(True)
            model.Export(os.path.dirname(os.path.realpath(__file__)) + "/../Models/Bayes.model")

            # Gradient Boosted Trees Classifier
            model = GradientBoostedTrees(["nb_clicks_1week", "product_price",
                                          "product_age_group", "device_type",
                                          "audience_id", "product_gender",
                                          "product_brand", "product_category1",
                                          "product_category2", "product_category3",
                                          "product_category4", "product_category5",
                                          "product_category6", "product_category7",
                                          "product_title"], "Sale", integrator, "classification")
            model.Train()
            # model.Load(os.path.dirname(os.path.realpath(__file__)) + "/../Models/GBTClass.model")
            predictions, accGBTClass = model.Predict(True)
            model.Export(os.path.dirname(os.path.realpath(__file__)) + "/../Models/GBTClass.model")

            # Gradient Boosted Trees Regression
            # Price prediction
            model = GradientBoostedTrees(["nb_clicks_1week", "product_price",
                                          "product_age_group", "device_type",
                                          "audience_id", "product_gender",
                                          "product_brand", "product_category1",
                                          "product_category2", "product_category3",
                                          "product_category4", "product_category5",
                                          "product_category6", "product_category7",
                                          "product_title"], "SalesAmnountInEuro", integrator, "regression")
            model.Train()
            # model.Load(os.path.dirname(os.path.realpath(__file__)) + "/../Models/GBTRegPrice.model")
            predictions, accGBTRegPrice = model.Predict(True)
            model.Export(os.path.dirname(os.path.realpath(__file__)) + "/../Models/GBTRegPrice.model")

            # Time prediction
            model = GradientBoostedTrees(["nb_clicks_1week", "product_price",
                                          "product_age_group", "device_type",
                                          "audience_id", "product_gender",
                                          "product_brand", "product_category1",
                                          "product_category2", "product_category3",
                                          "product_category4", "product_category5",
                                          "product_category6", "product_category7",
                                          "product_title"], "Time_delay_for_conversion", integrator, "regression")
            model.Train()
            # model.Load(os.path.dirname(os.path.realpath(__file__)) + "/../Models/GBTRegTime.model")
            predictions, accGBTRegTime = model.Predict(True)
            model.Export(os.path.dirname(os.path.realpath(__file__)) + "/../Models/GBTRegTime.model")

            # Linear Least Squares
            # Price prediction
            model = LinearLeastSquare(["nb_clicks_1week", "product_price",
                                       "product_age_group", "device_type",
                                       "audience_id", "product_gender",
                                       "product_brand", "product_category1",
                                       "product_category2", "product_category3",
                                       "product_category4", "product_category5",
                                       "product_category6", "product_category7",
                                       "product_title"], "SalesAmnountInEuro", integrator)
            model.Train()
            # model.Load(os.path.dirname(os.path.realpath(__file__)) + "/../Models/LinLSPrice.model")
            predictions, accLinLSPrice = model.Predict(True)
            model.Export(os.path.dirname(os.path.realpath(__file__)) + "/../Models/LinLSPrice.model")

            # Time prediction
            model = LinearLeastSquare(["nb_clicks_1week", "product_price",
                                       "product_age_group", "device_type",
                                       "audience_id", "product_gender",
                                       "product_brand", "product_category1",
                                       "product_category2", "product_category3",
                                       "product_category4", "product_category5",
                                       "product_category6", "product_category7",
                                       "product_title"], "Time_delay_for_conversion", integrator)
            model.Train()
            # model.Load(os.path.dirname(os.path.realpath(__file__)) + "/../Models/LinLSTime.model")
            predictions, accLinLSTime = model.Predict(True)
            model.Export(os.path.dirname(os.path.realpath(__file__)) + "/../Models/LinLSTime.model")


            print("+-----------------------------------------------+")
            print("|          Training Results Summary             |")
            print("+----------------------------+------------------+")
            print("|  Clssifier / Regressor     |      Accuracy    |")
            print("+----------------------------+------------------+")
            print("|  Decision Trees Model      |      {0:.2f}     |".format(accDecTree * 100.0))
            print("|  Linear SVM                |      {0:.2f}     |".format(accLinSVM * 100.0))
            print("|  Naive Bayes Classifier    |      {0:.2f}     |".format(accBayes * 100.0))
            print("|  Grad. Boost. Trees Cl.    |      {0:.2f}     |".format(accGBTClass * 100.0))
            print("|  Grad. Boost. Trees Reg. P |      {0:.2f}     |".format(accGBTRegPrice * 100.0))
            print("|  Grad. Boost. Trees Reg. T |      {0:.2f}     |".format(accGBTRegTime * 100.0))
            print("|  Linear Least Squares P    |      {0:.2f}     |".format(accLinLSPrice * 100.0))
            print("|  Linear Least Squares T    |      {0:.2f}     |".format(accLinLSTime * 100.0))
            print("+----------------------------+------------------+")

        else:
            if args.model is not None:
                if args.model.lower() == "dectree":
                    model = DecisionTreeModel(["nb_clicks_1week", "product_price",
                                               "product_age_group", "device_type",
                                               "audience_id", "product_gender",
                                               "product_brand", "product_category1",
                                               "product_category2", "product_category3",
                                               "product_category4", "product_category5",
                                               "product_category6", "product_category7",
                                               "product_title"], "Sale", integrator)

                elif args.model.lower() == "linsvm":
                    model = LinearSVM(["nb_clicks_1week", "product_price",
                                               "product_age_group", "device_type",
                                               "audience_id", "product_gender",
                                               "product_brand", "product_category1",
                                               "product_category2", "product_category3",
                                               "product_category4", "product_category5",
                                               "product_category6", "product_category7",
                                               "product_title"], "Sale", integrator)

                elif args.model.lower() == "bayes":
                    model = Bayes(["nb_clicks_1week", "product_price",
                                               "product_age_group", "device_type",
                                               "audience_id", "product_gender",
                                               "product_brand", "product_category1",
                                               "product_category2", "product_category3",
                                               "product_category4", "product_category5",
                                               "product_category6", "product_category7",
                                               "product_title"], "Sale", integrator)

                elif args.model.lower() == "gbtclass":
                    model = GradientBoostedTrees(["nb_clicks_1week", "product_price",
                                               "product_age_group", "device_type",
                                               "audience_id", "product_gender",
                                               "product_brand", "product_category1",
                                               "product_category2", "product_category3",
                                               "product_category4", "product_category5",
                                               "product_category6", "product_category7",
                                                            "product_title"], "Sale", integrator, "classification")

                elif args.model.lower() == "gbtregprice":
                    model = GradientBoostedTrees(["nb_clicks_1week", "product_price",
                                               "product_age_group", "device_type",
                                               "audience_id", "product_gender",
                                               "product_brand", "product_category1",
                                               "product_category2", "product_category3",
                                               "product_category4", "product_category5",
                                               "product_category6", "product_category7",
                                                  "product_title"], "SalesAmountInEuro", integrator, "regression")

                elif args.model.lower() == "gbtregtime":
                    model = GradientBoostedTrees(["nb_clicks_1week", "product_price",
                                               "product_age_group", "device_type",
                                               "audience_id", "product_gender",
                                               "product_brand", "product_category1",
                                               "product_category2", "product_category3",
                                               "product_category4", "product_category5",
                                               "product_category6", "product_category7",
                                                  "product_title"], "Time_delay_for_conversion", integrator, "regression")

                elif args.model.lower() == "linlsprice":
                    model = LinearLeastSquare(["nb_clicks_1week", "product_price",
                                               "product_age_group", "device_type",
                                               "audience_id", "product_gender",
                                               "product_brand", "product_category1",
                                               "product_category2", "product_category3",
                                               "product_category4", "product_category5",
                                               "product_category6", "product_category7",
                                               "product_title"], "SalesAmountInEuro", integrator)

                elif args.model.lower() == "linlstime":
                    model = LinearLeastSquare(["nb_clicks_1week", "product_price",
                                               "product_age_group", "device_type",
                                               "audience_id", "product_gender",
                                               "product_brand", "product_category1",
                                               "product_category2", "product_category3",
                                               "product_category4", "product_category5",
                                               "product_category6", "product_category7",
                                               "product_title"], "Time_delay_for_conversion", integrator)

                if args.method == "train":
                    model.Train()
                    print(model.Predict(True))
                    if args.modelOutput is not None:
                        model.Export(args.modelOutput)

                if args.method == "predict":
                    model.Load(args.modelInput)
                    model.Predict(False).show()

    print("All DONE! If you didn't get what you wanted you probably used wrong combination of arguments...")
    print("But if you did, we are glad to have served you. :)")

