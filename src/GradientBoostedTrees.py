from Integration import Integrator, PreProcess
from Model import Model
from pyspark.ml import Pipeline, PipelineModel
from pyspark.ml.regression import GBTRegressor
from pyspark.ml.classification import GBTClassifier
from pyspark.ml.feature import StringIndexer, VectorIndexer, VectorAssembler
from pyspark.ml.evaluation import RegressionEvaluator, MulticlassClassificationEvaluator
from Utilities import CalculateCorrelationCoefficient

class GradientBoostedTrees(Model):
    def __init__(self, features, labels, integrator, treeType):
        super(GradientBoostedTrees, self).__init__(features, labels)
        self.integrator = integrator
        self.treeType = treeType

        # We want to know how much did people spend only if they have finished the transaction
        if self.treeType == "regression":
            self.integrator.PreProcessed = PreProcess.GetPositiveRecords(integrator.PreProcessed)

        # For learning models, it is necessary to combine all input attributes into a single numeric vector using the 'VectorAssembler'
        self.data = VectorAssembler(inputCols = self.features, outputCol="features").transform(self.integrator.PreProcessed)
        # Index labels, adding metadata to the label column.
        # Fit on whole dataset to include all labels in index.
        self.labelIndexer = StringIndexer(inputCol=self.label, outputCol="indexedLabel").fit(self.data)

        # Automatically identify categorical features, and index them.
        # We specify maxCategories so features with > 4 distinct values are treated as continuous.
        self.featureIndexer = VectorIndexer(inputCol="features", outputCol="indexedFeatures", maxCategories=8).fit(self.data)

        # Split the data into training and test sets (30% held out for testing)
        (self.trainingData, self.testData) = self.data.randomSplit([0.7, 0.3])

        if self.treeType == "regression":
            self.GBT = GBTRegressor(labelCol="indexedLabel", featuresCol="indexedFeatures", maxMemoryInMB = 5000)
        elif self.treeType == "classification":
            self.GBT = GBTClassifier(labelCol="indexedLabel", featuresCol="indexedFeatures", maxMemoryInMB = 5000)

        self.pipeline = Pipeline(stages=[self.labelIndexer, self.featureIndexer, self.GBT])
        self.GBTModel = None

    def Train(self):
        self.GBTModel = self.pipeline.fit(self.trainingData)

    def Predict(self, evaluate):
        if self.treeType == "regression":
            if evaluate:
                predictions = self.GBTModel.transform(self.testData)
                evaluator = RegressionEvaluator(labelCol="indexedLabel",
                                                predictionCol="prediction",
                                                metricName="rmse")
                accuracy = evaluator.evaluate(predictions)
                return predictions.select("prediction", "indexedLabel", "features"), 1 - accuracy

            predictions = self.GBTModel.transform(self.data)
            return predictions.select("prediction", "indexedLabel", "features")

        elif self.treeType == "classification":
            if evaluate:
                predictions = self.GBTModel.transform(self.testData)
                accuracy = CalculateCorrelationCoefficient(predictions)
                return predictions.select("prediction", "indexedLabel", "features"), accuracy

            predictions = self.GBTModel.transform(self.data)
            return predictions.select("prediction", "indexedLabel", "features")

    def Export(self, filename):
        self.GBTModel.write().overwrite().save(filename)

    def Load(self, filename):
        self.GBTModel = PipelineModel.load(filename)
