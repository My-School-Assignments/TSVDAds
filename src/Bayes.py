from Model import Model
from pyspark.ml.classification import NaiveBayes, NaiveBayesModel
from pyspark.mllib.util import MLUtils
from Integration import Integrator
from pyspark.ml import Pipeline, PipelineModel
from pyspark.ml.feature import StringIndexer, VectorIndexer, VectorAssembler
from Utilities import CalculateCorrelationCoefficient

class Bayes(Model):
    def __init__(self, features, labels, integrator):
        super(Bayes, self).__init__(features,labels)
        self.integrator = integrator
        # For learning models, it is necessary to combine all input attributes into a single numeric vector using the 'VectorAssembler'
        self.data = VectorAssembler(inputCols = self.features, outputCol="features").transform(self.integrator.PreProcessed)
        # Index labels, adding metadata to the label column.
        # Fit on whole dataset to include all labels in index.
        self.labelIndexer = StringIndexer(inputCol=self.label, outputCol="indexedLabel").fit(self.data)

        # Automatically identify categorical features, and index them.
        # We specify maxCategories so features with > 4 distinct values are treated as continuous.
        self.featureIndexer = VectorIndexer(inputCol="features", outputCol="indexedFeatures", maxCategories=8).fit(self.data)

        # Split the data into training and test sets (30% held out for testing)
        (self.trainingData, self.testData) = self.data.randomSplit([0.7, 0.3])
        self.naiveBayes = NaiveBayes(labelCol="indexedLabel",featuresCol="indexedFeatures")
        self.pipeline = Pipeline(stages=[self.labelIndexer,self.featureIndexer,self.naiveBayes])
        self.naiveBayesModel = None

    def Train(self):
        self.naiveBayesModel = self.pipeline.fit(self.trainingData)

    def Predict(self, evaluate):
        if evaluate:
            predictions = self.naiveBayesModel.transform(self.testData)
            accuracy = CalculateCorrelationCoefficient(predictions)
            return predictions.select("prediction", "indexedLabel", "features"), accuracy

        predictions = self.naiveBayesModel.transform(self.data)
        return predictions.select("prediction", "indexedLabel", "features")

    def Export(self, filename):
        self.naiveBayesModel.write().overwrite().save(filename)

    def Load(self, filename):
        self.naiveBayesModel = PipelineModel.load(filename)
