# Utility scripts. Data transformations and misc 
# functions that will be needed should be here.

from __future__ import division
import pyspark.sql.functions as func
import math
import os, errno


class DataTypes:
    Download = "download"
    DownloadAndUnzip = "downloadandunzip"
    Load = "load"

def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno == errno.EISDIR:
            os.removedirs(filename)
        elif e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred


def CalculateCorrelationCoefficient(predictions):
    dataFrame = predictions.select(func.round(predictions["prediction"]).alias("prediction"),
                                func.col("indexedLabel").alias("indexedLabel"))
    predicted = dataFrame.select("prediction").rdd.flatMap(lambda x: x).collect()
    actual = dataFrame.select("indexedLabel").rdd.flatMap(lambda x: x).collect()
    TP = 0
    TN = 0
    FP = 0
    FN = 0


    for i in range(0, len(predicted)):
        if actual[i] == predicted[i]:
            if actual[i] == 1:
                TP += 1
            else:
                TN += 1
        if actual[i] > predicted[i]:
            FN += 1

        if actual[i] < predicted[i]:
            FP += 1

    MCC = ((TP * TN) - (FP * FN)) / math.sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN))
    return MCC