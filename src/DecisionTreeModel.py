# Decision Tree Model implementation using Spark MLLib

from Model import Model
from Integration import Integrator
from pyspark.ml import Pipeline, PipelineModel
from pyspark.ml.classification import DecisionTreeClassifier
from pyspark.ml.feature import StringIndexer, VectorIndexer, VectorAssembler
from Utilities import CalculateCorrelationCoefficient

class DecisionTreeModel (Model):
    def __init__(self, features, label, integrator):
        # type: (list, str, Integrator) -> None
        super(DecisionTreeModel, self).__init__(features, label)

        self.integrator = integrator
        # For learning models, it is necessary to combine all input attributes into a single numeric vector using the 'VectorAssembler'
        self.data = VectorAssembler(inputCols = self.features, outputCol="features").transform(self.integrator.PreProcessed)
        # Index labels, adding metadata to the label column.
        # Fit on whole dataset to include all labels in index.
        self.labelIndexer = StringIndexer(inputCol=self.label, outputCol="indexedLabel").fit(self.data)

        # Automatically identify categorical features, and index them.
        # We specify maxCategories so features with > 4 distinct values are treated as continuous.
        self.featureIndexer = VectorIndexer(inputCol="features", outputCol="indexedFeatures", maxCategories=8).fit(self.data)

        # Split the data into training and test sets (30% held out for testing)
        (self.trainingData, self.testData) = self.data.randomSplit([0.7, 0.3])
        # Train a DecisionTree model.
        self.dt = DecisionTreeClassifier(labelCol="indexedLabel", featuresCol="indexedFeatures", maxMemoryInMB = 5000)

        # Chain indexers and tree in a Pipeline
        self.pipeline = Pipeline(stages=[self.labelIndexer, self.featureIndexer, self.dt])
        self.model = None

    def Train(self):
        # Train model.  This also runs the indexers.
        self.model = self.pipeline.fit(self.trainingData)

    def Predict(self, evaluate = False):
        if evaluate:
            predictions = self.model.transform(self.testData)
            accuracy = CalculateCorrelationCoefficient(predictions)
            return predictions.select("prediction", "indexedLabel", "features"), accuracy

        predictions = self.model.transform(self.data)
        return predictions.select("prediction", "indexedLabel", "features")

    def Export(self, filename):
        self.model.write().overwrite().save(filename)

    def Load(self, filename):
        self.model = PipelineModel.load(filename)
