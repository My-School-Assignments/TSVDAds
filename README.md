﻿# Our Awesome Click-Buy predictor

This project was developed as school assignment for Technologies for Big Data Analysis class. The purpose of this project was to create distributed classifier and predictor using Apache pySpark. The system should predict whether user would buy a product after they clicked on the advertisment and how much they would buy based on historic data.

Data used for this assignment can be found here (hopefuly they won't be deleted since they are hosted on school website):
[**Click!**](http://web.tuke.sk/fei-cit/sarnovsky/tsvd/files/train.zip)

### Usage
```zsh
usage: main.py [-h] [-f DATAFILE] [-t DATATYPE] [-m METHOD] [-p]
               [-stat STATISTICS] [-samp] [-sdat SAVEDATA] [-i MODELINPUT]
               [-md MODEL] [-o MODELOUTPUT]

optional arguments:
  -h, --help            show this help message and exit
  -f DATAFILE, --dataFile DATAFILE
                        Path to data file(s)
  -t DATATYPE, --dataType DATATYPE
                        Type of data: load, download
  -m METHOD, --method METHOD
                        Method: train, predict, all
  -p, --preprocess      Set this if data you are loading are not preprocessed.
  -stat STATISTICS, --statistics STATISTICS
                        Compute statistics: specify path where to save results
  -samp, --sample       Set this if you want to sample data before
                        preprocessing.
  -sdat SAVEDATA, --savedata SAVEDATA
                        Path were to save preprocessed data.
  -i MODELINPUT, --modelInput MODELINPUT
                        Full filename path to already trained model.
  -md MODEL, --model MODEL
                        Model to use: DecTree, linsvm, bayes, gbtclass,
                        GBTRegPrice, GBTRegTime, LinLSPrice, LinLSTime
  -o MODELOUTPUT, --modelOutput MODELOUTPUT
                        Full filename path where to save trained model. If not
                        set the model will not be saved!
```

#### Examples

Get to project root dir:
```zsh
cd /home/..../our/Project/Dir/
```

To just Pre-Process data and save them as *.csv:
```zsh
spark-submit --master spark://147.232.202.81:7077 src/main.py -f "/path/to/original/data.csv" -p -samp -sdat "/path/where/to/save/data.csv"
```
    
To train all models (plus run Pre-Processing first automatically) and save them to Models/ directory (-samp is optional if you want to train only on 10% of all data):
```zsh
spark-submit --master spark://147.232.202.81:7077 src/main.py -f "/path/to/data.csv" -m all [-samp]
``` 

To train specific model (Decision Tree) and save it ... ASUMING data.csv are Pre-Processed if not use -p flag ... again -samp is optional if you want:
```zsh
spark-submit --master spark://147.232.202.81:7077 src/main.py -f "/path/to/data.csv" -m train -md DecTree -o "/path/for/saving/DecTree.model"
```

To load trained model and use it for predictions... ASUMING data.csv are Pre-Processed if not use -p flag :
```zsh
spark-submit --master spark://147.232.202.81:7077 src/main.py -f "/path/to/data.csv" -m predict -md DecTree -i "/path/to/trained/mode/DecTree.model"
```

The program supports loading data from web URL, we didn't test it but it should work. Also you can generate statistics from loaded data using -stat flag and providing path to file where to save them.

**There might be a combination that breaks something so please be careful when running program. It's still in BETA-like state and probably will stay that way. If you break something it's on you.**

### Note
If you want to use this code in any way please contact us first!

---
© Dominik Horňák, Richard Halčin, Tomáš Goffa, Ladislav Pomšár
